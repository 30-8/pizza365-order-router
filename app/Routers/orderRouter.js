const express = require("express");

const router = express.Router();

router.get("/orders", (request, response)=> {
    response.status(200).json({
        message: "GET All orders"
    })
});

router.get('/orders/:orderId', (request, response)=> {
    let orderId = request.params.orderId;
    response.status(200).json({

        message: "GET order Id = " + orderId
    })
});

router.post("/orders", (request, response)=> {
    response.status(200).json({
        message: "Create order"
    })
});

router.put('/orders/:orderId', (request, response)=> {
    let orderId = request.params.orderId;
    response.status(200).json({

        message: "Update order Id = " + orderId
    })
});

router.delete('/orders/:orderId', (request, response)=> {
    let orderId = request.params.orderId;
    response.status(200).json({

        message: "delete order Id = " + orderId
    })
});

module.exports = router;