//import thu vien express js 
const express = require('express');

//Khoi tao app Express 
const app = express();

//Khai bao cong chay project
const port = 3004;

//Import router Module
const orderRouter = require("./app/routers/orderRouter");

app.use((req,res,next) => {
    let today = new Date();

    console.log("Current: ", today);

    next();
})

app.use((req,res,next) =>{
    console.log("Methos: ", req.method);
    
    next();
})

//Callback function la mot function tham so cura mot function khac, no se thuc hien khi function day duoc goi
//Khai bao api dang /
app.get("/", (req, res) => {
    let today = new Date();

    res.status(200).json({
        message:  `Xin chao, hom nay la ngay ${today.getDate()} thang ${today.getMonth()} nam ${today.getFullYear()}`
    })
})

app.listen(port, () => {
    console.log('App listening on port', port);
});

app.use(orderRouter);

